/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class DicomStudyCStoreSamplerGUI extends AbstractSamplerGui {
    private static final long serialVersionUID = 1L;

    private final JLabel remoteHostLabel = new JLabel("XNAT hostname:"),
    remotePortLabel = new JLabel("XNAT DICOM port"),
    localAETitleLabel = new JLabel("Sender AE title:"),
    remoteAETitleLabel = new JLabel("Remote AE title"),
    templateFileLabel = new JLabel("path to sample DICOM file:"),
    fileCountLabel = new JLabel("series counts (comma-separated):");
    
    private final JTextField remoteHostField = new JTextField(),
    remotePortField = new JTextField(),
    localAETitleField = new JTextField(),
    remoteAETitleField = new JTextField(),
    templateFileField = new JTextField(),
    fileCountField = new JTextField();

    /**
     * 
     */
    public DicomStudyCStoreSamplerGUI() {
        setLayout(new BorderLayout(0, 5));
        setBorder(makeBorder());
        add(makeTitlePanel(), BorderLayout.NORTH);
        add(makeParameterPanel(), BorderLayout.CENTER);
    }

    private Component makeParameterPanel() {
        final JPanel panel = new JPanel(new GridLayout(6, 2));
        panel.add(remoteHostLabel);
        panel.add(remoteHostField);
        remoteHostLabel.setLabelFor(remoteHostField);
        panel.add(remotePortLabel);
        panel.add(remotePortField);
        remotePortLabel.setLabelFor(remotePortField);
        panel.add(localAETitleLabel);
        panel.add(localAETitleField);
        localAETitleLabel.setLabelFor(localAETitleField);
        panel.add(remoteAETitleLabel);
        panel.add(remoteAETitleField);
        remoteAETitleLabel.setLabelFor(remoteAETitleField);
        panel.add(templateFileLabel);
        panel.add(templateFileField);
        templateFileLabel.setLabelFor(templateFileField);
        panel.add(fileCountLabel);
        panel.add(fileCountField);
        fileCountLabel.setLabelFor(fileCountField);
        return panel;
    }

    public void configure(final TestElement te) {
        super.configure(te);
        final DicomStudyCStoreSampler sampler = (DicomStudyCStoreSampler)te;
        final URI uri;
        try {
            uri = new URI(sampler.getBaseURL() + "/" + sampler.getRequestURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        remoteHostField.setText(uri.getHost());
        remotePortField.setText(Integer.toString(uri.getPort()));
        localAETitleField.setText(uri.getUserInfo());
        remoteAETitleField.setText(uri.getPath().replaceFirst("/", ""));
        templateFileField.setText(sampler.getTemplate());
        fileCountField.setText(sampler.getCounts());
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#createTestElement()
     */
    public TestElement createTestElement() {
        final DicomStudyCStoreSampler sampler = new DicomStudyCStoreSampler();
        modifyTestElement(sampler);
        return sampler;
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#getLabelResource()
     */
    public String getLabelResource() {
        return "dicom_study_cstore_sampler";
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.gui.JMeterGUIComponent#modifyTestElement(org.apache.jmeter.testelement.TestElement)
     */
    public void modifyTestElement(final TestElement te) {
        super.configureTestElement(te);
        final DicomStudyCStoreSampler sampler = (DicomStudyCStoreSampler)te;
        final String base = String.format("dicom://%s@%s:%s",
                localAETitleField.getText(),
                remoteHostField.getText(),
                remotePortField.getText());
        sampler.setBaseURL(base);
        sampler.setImportURI(remoteAETitleField.getText());
        sampler.setTemplate(templateFileField.getText());
        sampler.setCounts(fileCountField.getText()); 
    }
}
