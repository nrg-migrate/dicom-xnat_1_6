/**
 * Copyright 2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;

import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.Utils;
import org.nrg.attr.Utils.NotRootDir;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.SOPModel;
import org.nrg.ulog.MicroLog;
import org.nrg.xdat.bean.CatDcmcatalogBean;
import org.nrg.xdat.bean.CatDcmentryBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Supplier;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class CatalogBuilder implements Callable<Map<XnatResourcecatalogBean,CatDcmcatalogBean>> {
    private static final String RESOURCE_LABEL = "DICOM";
    private static final String RESOURCE_LABEL_SECONDARY = "secondary";
    private static final String RESOURCE_FORMAT = "DICOM";
    private static final String RESOURCE_CONTENT = "RAW";
    private static final String RESOURCE_CONTENT_SECONDARY = "secondary";

    private static final Map<?,String> EMPTY_CONSTRAINTS = Collections.emptyMap();

    private final Logger logger = LoggerFactory.getLogger(CatalogBuilder.class);
    private final String id;
    private final MicroLog log;
    private final DicomMetadataStore store;
    private final File root;
    private final boolean shouldLoadFiles;
    private final Map<Object,String> constraints = Maps.newLinkedHashMap();

    private Integer nFrames = null;

    public CatalogBuilder(final String id, final MicroLog log,
            final DicomMetadataStore store, final File root,
            final Map<?,String> constraints,
            final boolean shouldLoadFiles) {
        this.id = id;
        this.log = log;
        this.store = store;
        this.root = root;
        if (null != constraints) {
            this.constraints.putAll(constraints);
        }
        this.shouldLoadFiles = shouldLoadFiles;
    }

    public CatalogBuilder(final String id, final MicroLog log,
            final DicomMetadataStore store, final File root,
            final boolean shouldLoadFiles) {
        this(id, log, store, root, EMPTY_CONSTRAINTS, shouldLoadFiles);
    }

    public CatalogBuilder(final String id, final MicroLog log,
            final DicomMetadataStore store, final File root,
            final Map<?,String> constraints) {
        this(id, log, store, root, constraints, false);
    }

    private static ListMultimap<File,ExtAttrValue>
    getSortedFileValues(final DicomMetadataStore store, final Map<?,String> constraints)
    throws IOException,SQLException {
        final AttrAdapter fileAttrs = new AttrAdapter(store, constraints);
        fileAttrs.add(ImageFileAttributes.get());
        final ListMultimap<File,ExtAttrValue> unsorted = fileAttrs.getValuesForFiles();
        if (unsorted.isEmpty()) {
            throw new FileNotFoundException("scan contains no image files");			
        }
        final ListMultimap<File,ExtAttrValue> sorted = Multimaps.newListMultimap(
                new TreeMap<File,Collection<ExtAttrValue>>(new ImageFileComparator(unsorted)),
                new Supplier<List<ExtAttrValue>>() {
                    public List<ExtAttrValue> get() { return Lists.newArrayList(); }
                });
        sorted.putAll(unsorted);
        final ListMultimap<File,ExtAttrValue> fileValues = ArrayListMultimap.create(sorted);
        return fileValues;
    }

    private List<ExtAttrValue>
    getCatalogValues(final Map<?,String> constraints) throws IOException {
        final AttrAdapter catalogAttrs = new AttrAdapter(store, constraints);
        catalogAttrs.add(CatalogAttributes.get());
        final Map<ExtAttrDef<DicomAttributeIndex>,Throwable> catalogFailures = Maps.newHashMap();
        final List<ExtAttrValue> catalogValues = org.nrg.session.SessionBuilder.getValues(catalogAttrs, catalogFailures);
        for (final Map.Entry<ExtAttrDef<DicomAttributeIndex>,Throwable> me: catalogFailures.entrySet()) {
            DICOMSessionBuilder.report(id, me.getKey(), me.getValue(), log);
        }
        return catalogValues;
    }

    public int getFrameCount() {
        if (null == nFrames) {
            throw new IllegalStateException("frame count not ready");
        } else synchronized (nFrames) {
            return nFrames;
        }
    }

    public Map<XnatResourcecatalogBean,CatDcmcatalogBean> call()
    throws IOException,SQLException {
        if (shouldLoadFiles) {
            final Map<String,String> addCols = Collections.singletonMap(SOPModel.XNAT_SCAN_COLUMN, id);
            store.add(Collections.singleton(root), addCols);
            constraints.putAll(addCols);
        }

        final ListMultimap<File,ExtAttrValue> fileValues = getSortedFileValues(store, constraints);
        final List<ExtAttrValue> catalogValues = getCatalogValues(constraints);

        // Build the catalog beans and resource records.
        final Map<XnatResourcecatalogBean,CatDcmcatalogBean> catalogs = Maps.newLinkedHashMap();
        try {
            final CatDcmcatalogBean catalog = new CatDcmcatalogBean();
            // Add an entry for each file to the catalog, and count total number of frames (z dimension)
            final Collection<File> secondaryFiles = Lists.newArrayList();
            nFrames = 0;
            synchronized (nFrames) {
                for (final File file : fileValues.keySet()) {
                    final List<ExtAttrValue> values = fileValues.get(file);
                    final ExtAttrValue sopAttr = values.remove(0);
                    assert ImageFileAttributes.SOPClassUID.equals(sopAttr.getName()) : "expected SOP Class UID, found " + sopAttr;
                    final String fileSOPClass = sopAttr.getText();
                    if (SOPModel.isPrimaryImagingSOP(fileSOPClass)) {
                        int frames = 1;		// one frame per file unless otherwise specified
                        final CatDcmentryBean entry = new CatDcmentryBean();
                        for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(entry, values,
                                ImageFileAttributes.SOPClassUID, "URI", "numFrames")) {
                            if (ImageFileAttributes.SOPClassUID.equals(val.getName())) {
                                // ignore; we're done with this attribute
                            } else if ("URI".equals(val.getName())) {
                                try {
                                    entry.setUri(Utils.getRelativeURI(root, file));
                                } catch (NotRootDir e) {
                                    logger.error("session directory not root for image file " + file, e);
                                }
                            } else {
                                assert "numFrames".equals(val.getName()) : "unexpected file attribute " + val.getName();
                                try {
                                    frames = Integer.valueOf(val.getText());
                                } catch (NumberFormatException e) {
                                    logger.warn("invalid frame count value " + val.getText() + " in " + file);
                                }
                            }
                        }
                        catalog.addEntries_entry(entry);
                        nFrames += frames;
                    } else {
                        secondaryFiles.add(file);
                    }
                }
            }
            if (nFrames > 0) {
                // There are some primary imaging data in this scan, so fill out
                // the primary catalog and build a resource for it.
                for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(catalog, catalogValues, "dimensions")) {
                    assert "dimensions".equals(val.getName());
                    // "dimensions" is set in both the series and the catalog.
                    // It's attributes-only and contains only "x" and "y";
                    // "z" must be computed from individual DICOM objects.
                    catalog.setDimensions_x(val.getAttrs().get("x"));
                    catalog.setDimensions_y(val.getAttrs().get("y"));
                }
                catalog.setDimensions_z(nFrames);

                final XnatResourcecatalogBean resource = new XnatResourcecatalogBean();
                resource.setLabel(RESOURCE_LABEL);
                resource.setFormat(RESOURCE_FORMAT);
                resource.setContent(RESOURCE_CONTENT);

                catalogs.put(resource, catalog);
            }

            // We might have some secondary files (i.e., those with different SOP class)
            // in the series as well
            if (!secondaryFiles.isEmpty()) {
                final CatDcmcatalogBean secondary = new CatDcmcatalogBean();
                for (final File f : secondaryFiles) {
                    final CatDcmentryBean entry = new CatDcmentryBean();
                    for (final ExtAttrValue val : org.nrg.session.SessionBuilder.setValues(entry, fileValues.get(f), "URI", "instanceNumber", "numFrames")) {
                        if ("URI".equals(val.getName())) {
                            try {
                                entry.setUri(Utils.getRelativeURI(root, f));
                            } catch (NotRootDir e) {
                                logger.error("session directory not root for image file " + f, e);
                            }
                        }
                        // ignore instanceNumber, numFrames, as they don't really apply to secondary files
                    }
                    secondary.addEntries_entry(entry);
                }
                final XnatResourcecatalogBean resource = new XnatResourcecatalogBean();
                resource.setLabel(RESOURCE_LABEL_SECONDARY);
                resource.setFormat(RESOURCE_FORMAT);
                resource.setContent(RESOURCE_CONTENT_SECONDARY);
                catalogs.put(resource, secondary);
            }

        } catch (IOException e) {
            log.log("Unable to save file for scan " + id, e);
        }

        return catalogs;
    }
}
