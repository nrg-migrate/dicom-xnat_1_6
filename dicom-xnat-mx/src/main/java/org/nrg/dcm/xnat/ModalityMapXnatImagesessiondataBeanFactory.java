/**
 * Copyright (c) 2012,2013 Washington University
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.Modality;

import java.util.Set;

import org.nrg.dcm.SOPModel;
import org.slf4j.Logger;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ModalityMapXnatImagesessiondataBeanFactory extends AttributeMapXnatImagesessiondataBeanFactory {
    private final static Function<Set<String>,String> LEAD_MODALITY = new Function<Set<String>,String>() {
        public String apply(final Set<String> modalities) {
            return SOPModel.getLeadModality(modalities);
        }
    };

    /**
      * @param logger
     */
    public ModalityMapXnatImagesessiondataBeanFactory(final Logger logger) {
        super(Modality, SOPModel.getModalityToSessionTypes(), LEAD_MODALITY, logger);
    }
}
