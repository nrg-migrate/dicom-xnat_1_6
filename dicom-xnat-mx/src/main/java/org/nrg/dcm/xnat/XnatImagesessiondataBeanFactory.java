/*
 * Copyright (c) 2012,2013 Washington University School of Medicine
 */
package org.nrg.dcm.xnat;

import org.nrg.dcm.DicomMetadataStore;
import org.nrg.xdat.bean.XnatImagesessiondataBean;

/**
 * DICOMSessionBuilder uses a chain of these Factories to build the scan bean.
 * The first one in the chain to succeed (i.e., return non-null) wins.
 * The factory class must have a constructor of the form:
 * public MySessiondataBeanFactory(org.slf4j.Logger logger)
 * where the logger will append to dcm_to_xnat.log
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public interface XnatImagesessiondataBeanFactory {
    /**
     * Attempt to create a XnatImagesessiondataBean, given the provided metadata.
     * @param store DicomMetadataStore containing the study metadata
     * @param studyInstanceUID study to be translated to a session
     * @return a XnatImagesessiondataBean (subclass) instance, of type determined by
     *  the study metadata; or null if this factory does not create sessions matching
     *  the provided study.
     */
    XnatImagesessiondataBean create(DicomMetadataStore store, String studyInstanceUID);
}